using TimeManagerEngine;
using UnityEngine;
using UnityEngine.UI;

namespace Creobit.EditionsUpgrade
{
    [RequireComponent(typeof(OpenPrefabService))]
    public class OfferButton : MonoBehaviour
    {
        [Header("Parameters")]
        [SerializeField]
        private bool _pauseGameAfterClick = false;
        [SerializeField]
        private bool _useLevelCheck = false;
        [Min(1)]
        [SerializeField]
        private int _freeLevelsCount = 6;

        [Header("Components")]
        [SerializeField]
        private Button _fakeButton;
        [SerializeField]
        private OpenPrefabService _openPrefabService;

#if TK2D
        [SerializeField] private tk2dUIItem _button;
#else
        [SerializeField] private Button _button;
#endif


        private void OnValidate()
        {
            if (_openPrefabService == null)
            {
                _openPrefabService = GetComponent<OpenPrefabService>();
            }
#if TK2D
            _button = GetComponent<tk2dUIItem>();

            if (TryGetComponent(out Collider collider))
            {
                collider.enabled = false;
            }
#endif
        }

        private void OnDestroy()
        {
            _fakeButton.onClick.RemoveListener(OnFakeButtonClicked);
        }

        private void Awake()
        {
            _fakeButton.onClick.AddListener(OnFakeButtonClicked);
        }

        private void PauseGame()
        {
#if TOYMAN
            AppTimer.Instance.gameTimeScale = 0.0f;
#elif ARGUNOV
            GameManager.I.IsPause = true;
#elif GAME_ON
            EmergencyCrewLevel.Instance.Paused = true;
#else
            Debug.LogError($"No handlers for pause game", gameObject);
#endif
        }

        private bool IsNeedToShowOffer()
        {
#if PREMIUM
    return false;
#endif

            bool gameIsPurchased = PlayerPrefs.HasKey("AllLevelsBuyKey") && PlayerPrefs.GetString("AllLevelsBuyKey") == "true";

            if (gameIsPurchased)
            {
                return false;
            }

            bool isFreeLevel = true;
#if TOYMAN
            isFreeLevel = Player.I.CurrentLevel <= _freeLevelsCount;
#elif ARGUNOV || GAME_ON
            isFreeLevel = PlayerPrefs.GetInt(UnlockLevelManager.LastVisitedLevelIndexKey, 0) <= _freeLevelsCount;
#else
            Debug.LogError($"No handlers for bool isFreeLevel", gameObject);
#endif
            if (_useLevelCheck && isFreeLevel)
            {
                return false;
            }

            return true;
        }

        private void OnFakeButtonClicked()
        {
            if (_pauseGameAfterClick)
            {
                PauseGame();
            }

            if (IsNeedToShowOffer())
            {
                _openPrefabService.OnClick();
            }
            else
            {
#if TK2D
                _button.SimulateClick();
#else
                _button.onClick.Invoke();
#endif
            }

        }
    }
}
