﻿using System;
using UnityEngine;
using UnityEngine.UI;

namespace Creobit.EditionsUpgrade
{
    [AddComponentMenu("Creobit/EditionsUpgrade/UnlockLevelItem")]
    public class UnlockLevelItem : MonoBehaviour
    {
        private const string UnlockedLevelKey = "UnlockedLevelKey";
#if TK2D
        [SerializeField] private tk2dUIItem _button;
        public tk2dUIItem Button => _button;
#else
        [SerializeField] private Button _button;
        public Button Button => _button;
#endif
        
        [SerializeField] private FakeLevelButton _fakeLevelButton;

        private int _index;
        public int Index => _index;
        
        private Button _fakeButton;

        public event Action<int> OnInteract;
        public event Action<int> OnUnlocked;
        public int TotalAdsCount { get; private set; }
        public int WatchedAdsCount { get; private set; }

        private bool _unlocked
        {
            get => PlayerPrefs.GetInt(UnlockedLevelKey + _index) > 0 || PlayerPrefs.HasKey(UnlockLevelManager.AllLevelsBuyKey);
            set
            {
                PlayerPrefs.SetInt(UnlockedLevelKey + _index, value ? 1 : 0);
                PlayerPrefs.Save();
            }
        }

        public bool Unlocked => _unlocked;

        private void OnValidate()
        {
#if TK2D
            _button ??= GetComponent<tk2dUIItem>();      
#else
            _button ??= GetComponent<Button>();
#endif
        }

        protected void Awake()
        {
            var fakeLevelObject = Instantiate(_fakeLevelButton, transform);
            _fakeButton = fakeLevelObject.FakeButton;
            _fakeButton.onClick.AddListener(InvokeInteraction);
        }

        private void InvokeInteraction()
        {
            OnInteract?.Invoke(_index);
        }

        public void Initialization(int index, int totalAds)
        {
            _index = index;
            TotalAdsCount = totalAds;
        }

        public void OnAddWatched()
        {
            WatchedAdsCount++;
            if (WatchedAdsCount >= TotalAdsCount)
            {
                OpenLevel();
            }
        }

        public void ResetWatchedAds()
        {
            if (!_unlocked)
            {
                WatchedAdsCount = 0;
            }
        }

        public void SetUnlockedStatus(bool state)
        {
            _unlocked = state;
        }
        
        public void OpenLevel()
        {
            _unlocked = true;
            OnUnlocked?.Invoke(_index);
            if (AnalyticsManager.I != null)
            {
                if (AnalyticsManager.I.autoLevelStartedAnalytics)
                    AnalyticsManager.I.LevelStarted(Index);
            }
            CallClick();
        }

        public void CallClick()
        {
#if TK2D
            _button.SimulateClick();
#else
            _button.onClick.Invoke();
#endif      
        }
    }
}