﻿using System;
using System.Linq;
#if TOYMAN
using TimeManagerEngine;
#endif
using UnityEngine;
using UnityEngine.UI;

namespace Creobit.EditionsUpgrade
{
    [AddComponentMenu("Creobit/EditionsUpgrade/PlayButton")]
    public class PlayButton : MonoBehaviour
    {
#if TK2D
        [SerializeField] private tk2dUIItem _button;
#else
        [SerializeField] private Button _button;
#endif
        
        private void OnValidate()
        {
#if TK2D
            _button ??= GetComponent<tk2dUIItem>();      
#else
            _button ??= GetComponent<Button>();
#endif
        }

        private enum ContinueType
        {
            LastUnlocked,
            LastVisited,
            LastClicked,
        }

        [SerializeField] private ContinueType _continueType;
        [SerializeField] private FakeLevelButton _fakeLevelButton;
    
        private UnlockLevelManager _unlockLevelManager;
        private Button _fakeButton;
        private int _continueIndex;

        protected void Awake()
        {
#if TK2D
            _button.enabled = false;
#endif
            _unlockLevelManager = FindObjectOfType<UnlockLevelManager>();
        
            var fakeLevelObject = Instantiate(_fakeLevelButton, transform);

            _fakeButton = fakeLevelObject.FakeButton;
            _fakeButton.onClick.AddListener(OnClickContinue);
        }

        private void OnEnable()
        {
#if TK2D
            _button.enabled = false;
#endif

            switch (_continueType)
            {
                default:
                case ContinueType.LastUnlocked:
                    _continueIndex = PlayerPrefs.GetInt(UnlockLevelManager.LastUnlockedLevelIndexKey);
                    break;
                case ContinueType.LastVisited:
                    _continueIndex = PlayerPrefs.GetInt(UnlockLevelManager.LastVisitedLevelIndexKey);
                    break;
                case ContinueType.LastClicked:
                    _continueIndex = PlayerPrefs.GetInt(UnlockLevelManager.LastUnlockedLevelIndexKey);
                    foreach (var level in _unlockLevelManager.UnlockLevels)
                    {
                        level.OnInteract += ChangeContinueOnInteract;
                    }
                    break;
            }
        }
        
        private void OnDisable()
        {
            if (_continueType == ContinueType.LastClicked)
            {
                foreach (var level in _unlockLevelManager.UnlockLevels)
                {
                    level.OnInteract -= ChangeContinueOnInteract;
                }
            }
        }
        
        private void ChangeContinueOnInteract(int levelId)
        {
#if TOYMAN
            if (levelId > Player.I.MaxAvailableLevel)
            {
                return;
            }
#endif  
            _continueIndex = levelId;
        }
        
        private void OnClickContinue()
        {
#if TK2D
            _button.enabled = false;
#endif
            if (_continueIndex<= 0)
            {
                _continueIndex = 1;
            }

            if (_unlockLevelManager == null)
            {
                _unlockLevelManager = FindObjectOfType<UnlockLevelManager>();
            }
#if TOYMAN
            var currentLevel = _unlockLevelManager.UnlockLevels.First(x => x.Button.GetComponent<MapLevelButton>().flag.activeSelf);
            Debug.Log(currentLevel.Index + " CURRENT");
#else
            var currentLevel = _unlockLevelManager.UnlockLevels.First(x => x.Index == _continueIndex);
#endif
            
            if (_unlockLevelManager.LevelCondition(currentLevel))
            {
                if (_continueType == ContinueType.LastClicked)
                {
                    _unlockLevelManager.UnlockLevelHasInteraction(_continueIndex);
                    
                    if (_unlockLevelManager.LevelInteraction == UnlockLevelManager.LevelInteractions.Unlocked)
                    {
                        currentLevel.OpenLevel();
                    }
                }
#if TK2D
                _button.SimulateClick();
#else
                _button.onClick.Invoke();
#endif
                return;
            }
            
        
            if (currentLevel.WatchedAdsCount < currentLevel.TotalAdsCount)
            {
#if TK2D
                _button.enabled = false;
#endif
                if (_continueType == ContinueType.LastClicked)
                {
                    _unlockLevelManager.UnlockLevelHasInteraction(_continueIndex);
                    if (_unlockLevelManager.LevelInteraction == UnlockLevelManager.LevelInteractions.Unlocked)
                    {
                        _unlockLevelManager.ShowUnlockWindow(currentLevel);
                    }
                }
                else
                {
                    _unlockLevelManager.ShowUnlockWindow(currentLevel);
                }
            }
            else
            {
                if (_continueType == ContinueType.LastClicked)
                {
                    _unlockLevelManager.UnlockLevelHasInteraction(_continueIndex);                    
                    if (_unlockLevelManager.LevelInteraction == UnlockLevelManager.LevelInteractions.Unlocked)
                    {
                        currentLevel.OpenLevel();
                    }
                }
#if TK2D
                _button.SimulateClick();
#else
                _button.onClick.Invoke();
#endif    
            }
        }
    }
}