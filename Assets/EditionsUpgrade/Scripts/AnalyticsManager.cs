using System.Collections.Generic;
#if UNITY_ANALYTICS
using Unity.Services.Analytics;
#endif
using Unity.Services.Core;
using UnityEngine;

namespace Creobit.EditionsUpgrade
{
    public class AnalyticsManager : MonoBehaviour
    {
        public bool ConsentIsGiven
        {
            get => PlayerPrefs.GetInt(SAVE_KEY_ENABLE_STATE, 1) > 0;
            set => PlayerPrefs.SetInt(SAVE_KEY_ENABLE_STATE, value ? 1 : 0);
        }
        
        private static AnalyticsManager _instance;

        public const string SAVE_KEY_ENABLE_STATE = "AnalyticsIsEnabled";

        public static AnalyticsManager I
        {
            get { return _instance; }
        }

        public bool autoLevelStartedAnalytics;

        private void Awake()
        {
#if !UNITY_IOS && !UNITY_ANDROID && !UNITY_STANDALONE_OSX && !UNITY_WSA
            DestroyImmediate(this);
            return;
#endif
#if UNITY_STANDALONE_OSX && !MAC_APPSTORE
            DestroyImmediate(this);
            return;
#endif
            if (_instance != null && _instance != this)
            {
                Destroy(this.gameObject);
                return;
            }

            _instance = this;
            DontDestroyOnLoad(this.gameObject);
        }

        public async void Start()
        {
            await UnityServices.InitializeAsync();
            ConsentGiven();
        }

        public void ConsentGiven()
        {
#if UNITY_ANALYTICS
            if (ConsentIsGiven)
            {

            AnalyticsService.Instance.StartDataCollection();
                Debug.Log("Start Data collection");
            }
#endif
        }

        public void TrophyPanelOpened()
        {
#if UNITY_ANALYTICS
            AnalyticsService.Instance.RecordEvent("ScreenTrophiesOpened");
            AnalyticsService.Instance.Flush();
#endif
        }

        public void SettingsPanelOpened()
        {
#if UNITY_ANALYTICS
            AnalyticsService.Instance.RecordEvent("ScreenSettingsOpened");
            AnalyticsService.Instance.Flush();
#endif
        }

        public void MapOpened()
        {
#if UNITY_ANALYTICS
            AnalyticsService.Instance.RecordEvent("ScreenMapOpened");
            AnalyticsService.Instance.Flush();
#endif
        }

        public void AllTrophiesWon()
        {
#if UNITY_ANALYTICS
            AnalyticsService.Instance.RecordEvent("AllTrophiesWon");
            AnalyticsService.Instance.Flush();
#endif
        }

        public void LevelStarted(int levelNumber)
        {
#if UNITY_ANALYTICS
            var levelStartedEvent = new CustomEvent("LevelStarted");
            levelStartedEvent.Add("levelNumber", levelNumber);
            AnalyticsService.Instance.RecordEvent(levelStartedEvent);
            AnalyticsService.Instance.Flush();
#endif
        }

        public void LevelRestarted(int levelNumber)
        {
#if UNITY_ANALYTICS
            var levelRestartedEvent = new CustomEvent("LevelRestarted");
            levelRestartedEvent.Add("levelNumber", levelNumber);
            AnalyticsService.Instance.RecordEvent(levelRestartedEvent);
            AnalyticsService.Instance.Flush();
#endif
        }

        public void LevelCompleted(int levelNumber, int stars)
        {
#if UNITY_ANALYTICS
            var levelCompletedEvent = new CustomEvent("LevelCompleted");
            levelCompletedEvent.Add("levelNumber", levelNumber);
            levelCompletedEvent.Add("stars", stars);
            AnalyticsService.Instance.RecordEvent(levelCompletedEvent);
            AnalyticsService.Instance.Flush();
#endif
        }
        
        public void InappPurchased(string place, int levelNumber)
        {
#if UNITY_ANALYTICS
            var inappPurchasedEvent = new CustomEvent("LevelCompleted");
            inappPurchasedEvent.Add("place", place);
            inappPurchasedEvent.Add("levelNumber", levelNumber);
            AnalyticsService.Instance.RecordEvent(inappPurchasedEvent);
            AnalyticsService.Instance.Flush();
#endif
        }
    }
}