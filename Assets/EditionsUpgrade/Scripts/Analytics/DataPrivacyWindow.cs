using System;
using System.Collections;
using System.Collections.Generic;
#if UNITY_ANALYTICS
using Unity.Services.Analytics;
#endif
using Unity.Services.Core;
using UnityEngine;

namespace Creobit.EditionsUpgrade
{
    [AddComponentMenu("Creobit/EditionsUpgrade/DataPrivacy/DataPrivacyWindow")]
    public class DataPrivacyWindow : MonoBehaviour
    {
        private void Start()
        {
#if !TK2D
            GetComponent<BoxCollider>().enabled = false;
#endif
#if UNITY_ANALYTICS
            if (UnityServices.State == ServicesInitializationState.Uninitialized)
            {
                UnityServices.InitializeAsync();
            }
#endif
        }

        public void OpenDataPrivacy()
        {
#if UNITY_ANALYTICS
            Application.OpenURL(AnalyticsService.Instance.PrivacyUrl);
#endif
        }
        
        public void DisableAnalytics()
        {
#if UNITY_ANALYTICS
            AnalyticsManager.I.ConsentIsGiven = false;
            AnalyticsService.Instance.StopDataCollection();
#endif
        }
        
        public void EnableAnalytics()
        {
#if UNITY_ANALYTICS
            AnalyticsManager.I.ConsentIsGiven = true;
            AnalyticsService.Instance.StartDataCollection();
#endif
        }
    }
}