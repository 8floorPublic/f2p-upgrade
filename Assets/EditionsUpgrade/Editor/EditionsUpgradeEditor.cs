using UnityEditor;
using UnityEditor.Build;
using UnityEngine;
using EditorUserBuildSettings = UnityEditor.EditorUserBuildSettings;

namespace EditionsUpgrade.Editor
{
    [InitializeOnLoad]
    public static class EditionsUpgradeEditor
    {
        private const string Tk2DMenuName = "8floor/EditionsUpgrade/TK2D";
        private const string DefinesCheckMenuName = "8floor/EditionsUpgrade/DefinesCheck";

#region EditionVariables
        
        // Prototype
        // private const string EditionMenuName = "8floor/EditionsUpgrade/Edition/edition_name";

        private const string EditionMenuName8FloorWindowsPremiumCe = "8floor/EditionsUpgrade/Edition/8floor_windows_premium_ce";
        private const string EditionMenuName8FloorWindowsPremiumSe = "8floor/EditionsUpgrade/Edition/8floor_windows_premium_se";
        private const string EditionMenuName8FloorMacOSPremiumCe = "8floor/EditionsUpgrade/Edition/8floor_macos_premium_ce";
        private const string EditionMenuName8FloorMacOSPremiumSe = "8floor/EditionsUpgrade/Edition/8floor_macos_premium_se";
        private const string EditionMenuNameSteamWindowsPremium = "8floor/EditionsUpgrade/Edition/steam_windows_premium";
        private const string EditionMenuNameSteamMacOSPremium = "8floor/EditionsUpgrade/Edition/steam_macos_premium";
        private const string EditionMenuNameMicrosoftWindowsPremium = "8floor/EditionsUpgrade/Edition/microsoft_windows_premium";
        private const string EditionMenuNameMicrosoftWindowsF2P = "8floor/EditionsUpgrade/Edition/microsoft_windows_f2p";
        private const string EditionMenuNameBfgWindowsPremiumCe = "8floor/EditionsUpgrade/Edition/bfg_windows_premium_ce";
        private const string EditionMenuNameBfgWindowsPremiumSe = "8floor/EditionsUpgrade/Edition/bfg_windows_premium_se";
        private const string EditionMenuNameBfgMacOSPremiumCe = "8floor/EditionsUpgrade/Edition/bfg_macos_premium_ce";
        private const string EditionMenuNameBfgMacOSPremiumSe = "8floor/EditionsUpgrade/Edition/bfg_macos_premium_se";
        private const string EditionMenuName8FloorAndroidPremiumCe = "8floor/EditionsUpgrade/Edition/8floor_android_premium_ce";
        private const string EditionMenuNameGooglePlayAndroidF2P = "8floor/EditionsUpgrade/Edition/googleplay_android_f2p";
        private const string EditionMenuNameGooglePlayAndroidPremium = "8floor/EditionsUpgrade/Edition/googleplay_android_premium";
        private const string EditionMenuNameAppstoreIosPremium = "8floor/EditionsUpgrade/Edition/appstore_ios_premium";
        private const string EditionMenuNameAppstoreIosF2P = "8floor/EditionsUpgrade/Edition/appstore_ios_f2p";
        private const string EditionMenuNameAppstoreMacosPremium = "8floor/EditionsUpgrade/Edition/appstore_macos_premium";
        private const string EditionMenuNameAppstoreMacosF2P = "8floor/EditionsUpgrade/Edition/appstore_macos_f2p";
        private const string EditionMenuNameEgsWindowsPremium = "8floor/EditionsUpgrade/Edition/egs_windows_premium";
        private const string EditionMenuNameHuaweiAndroidF2P = "8floor/EditionsUpgrade/Edition/huawei_android_f2p";
        private const string EditionMenuNameHuaweiAndroidPremium = "8floor/EditionsUpgrade/Edition/huawei_android_premium";
        
        // Prototype
        // private static bool _enabledEditionMenuName;
        
        private static bool _enabledTk2D;
        private static bool _enabled8FloorWindowsPremiumCe;
        private static bool _enabled8FloorMacOSPremiumCe;
        private static bool _enabled8FloorWindowsPremiumSe;
        private static bool _enabled8FloorMacOSPremiumSe;
        private static bool _enabledSteamWindowsPremium;
        private static bool _enabledSteamMacOSPremium;
        private static bool _enabledMicrosoftWindowsPremium;
        private static bool _enabledMicrosoftWindowsF2P;
        private static bool _enabledBfgWindowsPremiumCe;
        private static bool _enabledBfgMacOSPremiumCe;
        private static bool _enabledBfgWindowsPremiumSe;
        private static bool _enabledBfgMacOSPremiumSe;
        private static bool _enabledAndroidPremiumCeBuild;
        private static bool _enabledGooglePlayAndroidF2PBuild;
        private static bool _enabledGooglePlayAndroidPremiumBuild;
        private static bool _enabledAppstoreIosPremiumBuild;
        private static bool _enabledAppstoreIosF2P;
        private static bool _enabledAppstoreMacosPremiumBuild;
        private static bool _enabledAppstoreMacosF2P;
        private static bool _enabledEgsWindowsPremium;
        private static bool _enabledHuaweiAndroidF2P;
        private static bool _enabledHuaweiAndroidPremium;

#endregion

        private static string _allDefines;
        private static string _otherDefines;

#region Defines
        
        // Prototype
        // private static readonly string NameDefine = "DEFINE_NAME";
        
        private static readonly string Tk2DDefine = "TK2D";
        private static readonly string UdpDefine = "UDP";
        private static readonly string GooglePlayDefine = "GOOGLE_PLAY";
        private static readonly string PremiumDefine = "PREMIUM";
        private static readonly string CollectorDefine = "COLLECTOR";
        private static readonly string HasAdDefine = "HAS_AD";
        private static readonly string UnityAnalyticsDefine = "UNITY_ANALYTICS";
        private static readonly string MacAppstoreDefine = "MAC_APPSTORE";
        private static readonly string HuaweiDefine = "HUAWEI";

#endregion
        
        static EditionsUpgradeEditor() {

            _enabledTk2D = EditorPrefs.GetBool(Tk2DMenuName, false);
            _allDefines = PlayerSettings.GetScriptingDefineSymbolsForGroup(EditorUserBuildSettings
                .selectedBuildTargetGroup);
        }

#region MenuItems
        
        [MenuItem(Tk2DMenuName)]
        private static void Tk2Toggle()
        {
            _enabledTk2D = !_enabledTk2D;
            EditorPrefs.SetBool(Tk2DMenuName, _enabledTk2D);
            SetTk2DDefine(_enabledTk2D);
        }

        [MenuItem(Tk2DMenuName, true)]
        private static bool Tk2DToggleValidate()
        {
            Menu.SetChecked(Tk2DMenuName ,_enabledTk2D);
            Debug.Log(_enabledTk2D);
            return true;
        }
        
        [MenuItem(EditionMenuName8FloorWindowsPremiumCe)]
        private static void WindowsPremiumToggle()
        {
            DisableAllEditions();
            _enabled8FloorWindowsPremiumCe = true;
            EditorPrefs.SetBool(EditionMenuName8FloorWindowsPremiumCe, _enabled8FloorWindowsPremiumCe);
            Enable8FloorWindowsPremiumCeDefines(true);
        }

        [MenuItem(EditionMenuName8FloorWindowsPremiumCe, true)]
        private static bool WindowsPremiumToggleValidate()
        {
            Menu.SetChecked(EditionMenuName8FloorWindowsPremiumCe, _enabled8FloorWindowsPremiumCe);
            return true;
        }
        
        [MenuItem(EditionMenuNameSteamWindowsPremium)]
        private static void SteamWindowsPremiumToggle()
        {
            DisableAllEditions();
            _enabledSteamWindowsPremium = true;
            EditorPrefs.SetBool(EditionMenuNameSteamWindowsPremium, _enabledSteamWindowsPremium);
            Enable8FloorWindowsPremiumCeDefines(true);
        }

        [MenuItem(EditionMenuNameSteamWindowsPremium, true)]
        private static bool SteamWindowsPremiumToggleValidate()
        {
            Menu.SetChecked(EditionMenuNameSteamWindowsPremium, _enabledSteamWindowsPremium);
            return true;
        }
        
        [MenuItem(EditionMenuNameMicrosoftWindowsPremium)]
        private static void MicrosoftWindowsPremiumToggle()
        {
            DisableAllEditions();
            _enabledMicrosoftWindowsPremium = true;
            EditorPrefs.SetBool(EditionMenuNameMicrosoftWindowsPremium, _enabledMicrosoftWindowsPremium);
            Enable8FloorWindowsPremiumCeDefines(true);
        }

        [MenuItem(EditionMenuNameMicrosoftWindowsPremium, true)]
        private static bool MicrosoftWindowsPremiumToggleValidate()
        {
            Menu.SetChecked(EditionMenuNameMicrosoftWindowsPremium, _enabledMicrosoftWindowsPremium);
            return true;
        }
        
        [MenuItem(EditionMenuNameMicrosoftWindowsF2P)]
        private static void MicrosoftWindowsF2PToggle()
        {
            DisableAllEditions();
            _enabledMicrosoftWindowsF2P = true;
            EditorPrefs.SetBool(EditionMenuNameMicrosoftWindowsF2P, _enabledMicrosoftWindowsF2P);
            Enable8FloorWindowsPremiumCeDefines(true);
        }

        [MenuItem(EditionMenuNameMicrosoftWindowsF2P, true)]
        private static bool MicrosoftWindowsF2PToggleValidate()
        {
            Menu.SetChecked(EditionMenuNameMicrosoftWindowsF2P, _enabledMicrosoftWindowsF2P);
            return true;
        }
        
        [MenuItem(EditionMenuName8FloorWindowsPremiumSe)]
        private static void WindowsPremiumSeToggle()
        {
            DisableAllEditions();
            _enabled8FloorWindowsPremiumSe = true;
            EditorPrefs.SetBool(EditionMenuName8FloorWindowsPremiumSe, _enabled8FloorWindowsPremiumSe);
            Enable8FloorWindowsPremiumCeDefines(false);
        }

        [MenuItem(EditionMenuName8FloorWindowsPremiumSe, true)]
        private static bool WindowsPremiumSeToggleValidate()
        {
            Menu.SetChecked(EditionMenuName8FloorWindowsPremiumSe, _enabled8FloorWindowsPremiumSe);
            return true;
        }
        
        [MenuItem(EditionMenuNameBfgWindowsPremiumCe)]
        private static void BfgWindowsPremiumToggle()
        {
            DisableAllEditions();
            _enabledBfgWindowsPremiumCe = true;
            EditorPrefs.SetBool(EditionMenuNameBfgWindowsPremiumCe, _enabledBfgWindowsPremiumCe);
            Enable8FloorWindowsPremiumCeDefines(true);
        }

        [MenuItem(EditionMenuNameBfgWindowsPremiumCe, true)]
        private static bool BfgWindowsPremiumToggleValidate()
        {
            Menu.SetChecked(EditionMenuNameBfgWindowsPremiumCe, _enabledBfgWindowsPremiumCe);
            return true;
        }
        
        [MenuItem(EditionMenuNameBfgWindowsPremiumSe)]
        private static void BfgWindowsPremiumSeToggle()
        {
            DisableAllEditions();
            _enabledBfgWindowsPremiumSe = true;
            EditorPrefs.SetBool(EditionMenuNameBfgWindowsPremiumSe, _enabledBfgWindowsPremiumSe);
            Enable8FloorWindowsPremiumCeDefines(false);
        }

        [MenuItem(EditionMenuNameBfgWindowsPremiumSe, true)]
        private static bool BfgWindowsPremiumSeToggleValidate()
        {
            Menu.SetChecked(EditionMenuNameBfgWindowsPremiumSe, _enabledBfgWindowsPremiumSe);
            return true;
        }
        
        // This method will be called when user pressed the button in menu
        [MenuItem(EditionMenuNameEgsWindowsPremium)]
        private static void EgsWindowsPremiumToggle()
        {
            // Disable all other editions
            DisableAllEditions();
            // Set current edition to true and automatically call Validate method to set checked mark on this edition
            _enabledEgsWindowsPremium = true;
            EditorPrefs.SetBool(EditionMenuNameEgsWindowsPremium, _enabledEgsWindowsPremium);
            // Write edition specific instructions
            Enable8FloorWindowsPremiumCeDefines(true);
        }

        [MenuItem(EditionMenuNameEgsWindowsPremium, true)]
        private static bool EgsWindowsPremiumToggleValidate()
        {
            Menu.SetChecked(EditionMenuNameEgsWindowsPremium, _enabledEgsWindowsPremium);
            return true;
        }
        
        [MenuItem(EditionMenuName8FloorMacOSPremiumCe)]
        private static void MacOSPremiumToggle()
        {
            DisableAllEditions();
            _enabled8FloorMacOSPremiumCe = true;
            EditorPrefs.SetBool(EditionMenuName8FloorMacOSPremiumCe, _enabled8FloorMacOSPremiumCe);
            EnableMacOSPremiumDefines(true);
        }

        [MenuItem(EditionMenuName8FloorMacOSPremiumCe, true)]
        private static bool MacOSPremiumToggleValidate()
        {
            Menu.SetChecked(EditionMenuName8FloorMacOSPremiumCe, _enabled8FloorMacOSPremiumCe);
            return true;
        }
        
        [MenuItem(EditionMenuNameSteamMacOSPremium)]
        private static void SteamMacOSPremiumToggle()
        {
            DisableAllEditions();
            _enabledSteamMacOSPremium = true;
            EditorPrefs.SetBool(EditionMenuNameSteamMacOSPremium, _enabledSteamMacOSPremium);
            EnableMacOSPremiumDefines(true);
        }

        [MenuItem(EditionMenuNameSteamMacOSPremium, true)]
        private static bool SteamMacOSPremiumToggleValidate()
        {
            Menu.SetChecked(EditionMenuNameSteamMacOSPremium, _enabledSteamMacOSPremium);
            return true;
        }
        
        [MenuItem(EditionMenuName8FloorMacOSPremiumSe)]
        private static void MacOSPremiumSeToggle()
        {
            DisableAllEditions();
            _enabled8FloorMacOSPremiumSe = true;
            EditorPrefs.SetBool(EditionMenuName8FloorMacOSPremiumSe, _enabled8FloorMacOSPremiumSe);
            EnableMacOSPremiumDefines(false);
        }

        [MenuItem(EditionMenuName8FloorMacOSPremiumSe, true)]
        private static bool MacOSPremiumSeToggleValidate()
        {
            Menu.SetChecked(EditionMenuName8FloorMacOSPremiumSe, _enabled8FloorMacOSPremiumSe);
            return true;
        }
        
        [MenuItem(EditionMenuNameBfgMacOSPremiumCe)]
        private static void MacOSBfgPremiumToggle()
        {
            DisableAllEditions();
            _enabledBfgMacOSPremiumCe = true;
            EditorPrefs.SetBool(EditionMenuNameBfgMacOSPremiumCe, _enabledBfgMacOSPremiumCe);
            EnableMacOSPremiumDefines(true);
        }

        [MenuItem(EditionMenuNameBfgMacOSPremiumCe, true)]
        private static bool MacOSBfgPremiumToggleValidate()
        {
            Menu.SetChecked(EditionMenuNameBfgMacOSPremiumCe, _enabledBfgMacOSPremiumCe);
            return true;
        }
        
        [MenuItem(EditionMenuNameBfgMacOSPremiumSe)]
        private static void MacOSBfgPremiumSeToggle()
        {
            DisableAllEditions();
            _enabledBfgMacOSPremiumSe = true;
            EditorPrefs.SetBool(EditionMenuNameBfgMacOSPremiumSe, _enabledBfgMacOSPremiumSe);
            EnableMacOSPremiumDefines(false);
        }

        [MenuItem(EditionMenuNameBfgMacOSPremiumSe, true)]
        private static bool MacOSBfgPremiumSeToggleValidate()
        {
            Menu.SetChecked(EditionMenuNameBfgMacOSPremiumSe, _enabledBfgMacOSPremiumSe);
            return true;
        }
        
        [MenuItem(EditionMenuNameGooglePlayAndroidF2P)]
        private static void AndroidF2PToggle()
        {
            DisableAllEditions();
            _enabledGooglePlayAndroidF2PBuild = true;
            EditorPrefs.SetBool(EditionMenuNameGooglePlayAndroidF2P, _enabledGooglePlayAndroidF2PBuild);
            EnableGooglePlayAndroidF2PDefines();
        }

        [MenuItem(EditionMenuNameGooglePlayAndroidF2P, true)]
        private static bool AndroidF2PToggleValidate()
        {
            Menu.SetChecked(EditionMenuNameGooglePlayAndroidF2P, _enabledGooglePlayAndroidF2PBuild);
            return true;
        }
        
        [MenuItem(EditionMenuNameGooglePlayAndroidPremium)]
        private static void GooglePlayAndroidPremiumToggle()
        {
            DisableAllEditions();
            _enabledGooglePlayAndroidPremiumBuild = true;
            EditorPrefs.SetBool(EditionMenuNameGooglePlayAndroidPremium, _enabledGooglePlayAndroidPremiumBuild);
            EnableGooglePlayAndroidPremiumDefines();
        }

        [MenuItem(EditionMenuNameGooglePlayAndroidPremium, true)]
        private static bool GooglePlayAndroidPremiumToggleValidate()
        {
            Menu.SetChecked(EditionMenuNameGooglePlayAndroidPremium, _enabledGooglePlayAndroidPremiumBuild);
            Debug.Log(_enabledGooglePlayAndroidPremiumBuild);
            return true;
        }
        
        [MenuItem(EditionMenuNameAppstoreIosPremium)]
        private static void AppstoreIosPremiumToggle()
        {
            DisableAllEditions();
            _enabledAppstoreIosPremiumBuild = true;
            EditorPrefs.SetBool(EditionMenuNameAppstoreIosPremium, _enabledAppstoreIosPremiumBuild);
            EnableAppstoreIosPremiumDefines();
        }

        [MenuItem(EditionMenuNameAppstoreIosPremium, true)]
        private static bool AppstoreIosPremiumToggleValidate()
        {
            Menu.SetChecked(EditionMenuNameAppstoreIosPremium, _enabledAppstoreIosPremiumBuild);
            Debug.Log(_enabledAppstoreIosPremiumBuild);
            return true;
        }
        
        [MenuItem(EditionMenuNameAppstoreIosF2P)]
        private static void AppstoreIosF2PToggle()
        {
            DisableAllEditions();
            _enabledAppstoreIosF2P = true;
            EditorPrefs.SetBool(EditionMenuNameAppstoreIosF2P, _enabledAppstoreIosF2P);
            EnableAppstoreIosF2PDefines();
        }

        [MenuItem(EditionMenuNameAppstoreIosF2P, true)]
        private static bool AppstoreIosF2PToggleValidate()
        {
            Menu.SetChecked(EditionMenuNameAppstoreIosF2P, _enabledAppstoreIosF2P);
            return true;
        }
        
        [MenuItem(EditionMenuNameAppstoreMacosPremium)]
        private static void AppstoreMacosPremiumToggle()
        {
            DisableAllEditions();
            _enabledAppstoreMacosPremiumBuild = true;
            EditorPrefs.SetBool(EditionMenuNameAppstoreMacosPremium, _enabledAppstoreMacosPremiumBuild);
            EnableAppstoreMacosPremiumDefines();
        }

        [MenuItem(EditionMenuNameAppstoreMacosPremium, true)]
        private static bool AppstoreMacosPremiumToggleValidate()
        {
            Menu.SetChecked(EditionMenuNameAppstoreMacosPremium, _enabledAppstoreMacosPremiumBuild);
            return true;
        }
        
        [MenuItem(EditionMenuNameAppstoreMacosF2P)]
        private static void AppstoreMacosF2PToggle()
        {
            DisableAllEditions();
            _enabledAppstoreMacosF2P = true;
            EditorPrefs.SetBool(EditionMenuNameAppstoreMacosF2P, _enabledAppstoreMacosF2P);
            EnableAppstoreMacosF2PDefines();
        }

        [MenuItem(EditionMenuNameAppstoreMacosF2P, true)]
        private static bool AppstoreMacosF2PToggleValidate()
        {
            Menu.SetChecked(EditionMenuNameAppstoreMacosF2P, _enabledAppstoreMacosF2P);
            return true;
        }
        
        [MenuItem(EditionMenuName8FloorAndroidPremiumCe)]
        private static void Android8FloorPremiumCeToggle()
        {
            DisableAllEditions();
            _enabledAndroidPremiumCeBuild = true;
            EditorPrefs.SetBool(EditionMenuName8FloorAndroidPremiumCe, _enabledAndroidPremiumCeBuild);
            Enable8FloorAndroidPremiumDefines();
        }

        [MenuItem(EditionMenuName8FloorAndroidPremiumCe, true)]
        private static bool Android8FloorPremiumCeToggleValidate()
        {
            Menu.SetChecked(EditionMenuName8FloorAndroidPremiumCe, _enabledAndroidPremiumCeBuild);
            Debug.Log(_enabledAndroidPremiumCeBuild);
            return true;
        }
        [MenuItem(EditionMenuNameHuaweiAndroidF2P)]
        private static void HuaweiAndroidF2PCeToggle()
        {
            DisableAllEditions();
            _enabledHuaweiAndroidF2P = true;
            EditorPrefs.SetBool(EditionMenuNameHuaweiAndroidF2P, _enabledHuaweiAndroidF2P);
            EnableHuaweiAndroidF2PDefines();
        }

        [MenuItem(EditionMenuNameHuaweiAndroidF2P, true)]
        private static bool HuaweiAndroidF2PToggleValidate()
        {
            Menu.SetChecked(EditionMenuNameHuaweiAndroidF2P, _enabledHuaweiAndroidF2P);
            Debug.Log(_enabledHuaweiAndroidF2P);
            return true;
        }
        [MenuItem(EditionMenuNameHuaweiAndroidPremium)]
        private static void HuaweiAndroidPremiumCeToggle()
        {
            DisableAllEditions();
            _enabledHuaweiAndroidPremium = true;
            EditorPrefs.SetBool(EditionMenuNameHuaweiAndroidPremium, _enabledHuaweiAndroidPremium);
            EnableHuaweiAndroidPremiumDefines();
        }

        [MenuItem(EditionMenuNameHuaweiAndroidPremium, true)]
        private static bool HuaweiAndroidPremiumToggleValidate()
        {
            Menu.SetChecked(EditionMenuNameHuaweiAndroidPremium, _enabledHuaweiAndroidPremium);
            Debug.Log(_enabledHuaweiAndroidPremium);
            return true;
        }
        
#endregion
        
        private static void SetTk2DDefine(bool enabled)
        {
            if (AssetDatabase.FindAssets("TK2DROOT", null).Length == 0)
            {
                Debug.Log("TK2D wasn't found in project.");
                return;
            }

            Debug.Log("TK2DROOT exist");
            SetDefine(Tk2DDefine, enabled);
            PlayerSettings.SetScriptingDefineSymbolsForGroup(EditorUserBuildSettings.selectedBuildTargetGroup, _allDefines);


        }

        private static void Enable8FloorWindowsPremiumCeDefines(bool ce)
        {
            EditorUserBuildSettings.SwitchActiveBuildTarget(BuildTarget.StandaloneWindows64); 
            DisableAllOurDefines();
            SetDefine(PremiumDefine, true);
            if (ce)
                SetDefine(CollectorDefine, true);
            PlayerSettings.SetScriptingDefineSymbols(NamedBuildTarget.Standalone, _allDefines);
        }

        private static void EnableMacOSPremiumDefines( bool ce)
        {
            EditorUserBuildSettings.SwitchActiveBuildTarget(BuildTarget.StandaloneOSX);
            DisableAllOurDefines();
            SetDefine(PremiumDefine, true);
            if (ce)
                SetDefine(CollectorDefine, true);
            PlayerSettings.SetScriptingDefineSymbols(NamedBuildTarget.Standalone, _allDefines);
        }

        private static void Enable8FloorAndroidPremiumDefines()
        {
            EditorUserBuildSettings.SwitchActiveBuildTarget(BuildTarget.Android);
            DisableAllOurDefines();
            SetDefine(PremiumDefine, true);
            SetDefine(CollectorDefine, true);
            PlayerSettings.SetScriptingDefineSymbols(NamedBuildTarget.Android, _allDefines);
        }
        
        private static void EnableGooglePlayAndroidF2PDefines()
        {
            EditorUserBuildSettings.SwitchActiveBuildTarget(BuildTarget.Android);
            DisableAllOurDefines();
            SetDefine(GooglePlayDefine, true);
            SetDefine(HasAdDefine, true);
            SetDefine(UnityAnalyticsDefine, true);
            PlayerSettings.SetScriptingDefineSymbols(NamedBuildTarget.Android, _allDefines);
        }
        
        private static void EnableGooglePlayAndroidPremiumDefines()
        {
            EditorUserBuildSettings.SwitchActiveBuildTarget(BuildTarget.Android);
            DisableAllOurDefines();
            SetDefine(PremiumDefine, true);
            SetDefine(GooglePlayDefine, true);
            SetDefine(UnityAnalyticsDefine, true);
            PlayerSettings.SetScriptingDefineSymbols(NamedBuildTarget.Android, _allDefines);
        }
        
        private static void EnableUdpAndroidF2PDefines()
        {
            EditorUserBuildSettings.SwitchActiveBuildTarget(BuildTarget.Android);
            DisableAllOurDefines();
            SetDefine(UdpDefine, true);
            SetDefine(HasAdDefine, true);
            SetDefine(UnityAnalyticsDefine, true);
            PlayerSettings.SetScriptingDefineSymbols(NamedBuildTarget.Android, _allDefines);
        }
        
        private static void EnableUdpAndroidPremiumDefines()
        {
            EditorUserBuildSettings.SwitchActiveBuildTarget(BuildTarget.Android);
            DisableAllOurDefines();
            SetDefine(PremiumDefine, true);
            SetDefine(UdpDefine, true);
            SetDefine(UnityAnalyticsDefine, true);
            PlayerSettings.SetScriptingDefineSymbols(NamedBuildTarget.Android, _allDefines);
        }
        
        private static void EnableAppstoreIosPremiumDefines()
        {
            EditorUserBuildSettings.SwitchActiveBuildTarget(BuildTarget.iOS);
            DisableAllOurDefines();
            SetDefine(PremiumDefine, true);
            SetDefine(UnityAnalyticsDefine, true);
            PlayerSettings.SetScriptingDefineSymbols(NamedBuildTarget.iOS, _allDefines);
        }
        
        private static void EnableAppstoreIosF2PDefines()
        {
            EditorUserBuildSettings.SwitchActiveBuildTarget(BuildTarget.iOS);
            DisableAllOurDefines();
            SetDefine(HasAdDefine, true);
            SetDefine(UnityAnalyticsDefine, true);
            PlayerSettings.SetScriptingDefineSymbols(NamedBuildTarget.iOS, _allDefines);
        }
        
        private static void EnableAppstoreMacosPremiumDefines()
        {
            EditorUserBuildSettings.SwitchActiveBuildTarget(BuildTarget.StandaloneOSX);
            DisableAllOurDefines();
            SetDefine(PremiumDefine, true);
            SetDefine(CollectorDefine, true);
            SetDefine(UnityAnalyticsDefine, true);
            SetDefine(MacAppstoreDefine, true);
            PlayerSettings.SetScriptingDefineSymbols(NamedBuildTarget.Standalone, _allDefines);
        }
        
        private static void EnableAppstoreMacosF2PDefines()
        {
            EditorUserBuildSettings.SwitchActiveBuildTarget(BuildTarget.StandaloneOSX);
            DisableAllOurDefines();
            SetDefine(HasAdDefine, true);
            SetDefine(CollectorDefine, true);
            SetDefine(UnityAnalyticsDefine, true);
            SetDefine(MacAppstoreDefine, true);
            PlayerSettings.SetScriptingDefineSymbols(NamedBuildTarget.Standalone, _allDefines);
        }
        
        private static void EnableHuaweiAndroidF2PDefines()
        {
            EditorUserBuildSettings.SwitchActiveBuildTarget(BuildTarget.Android);
            DisableAllOurDefines();
            SetDefine(HuaweiDefine, true);
            SetDefine(HasAdDefine, true);
            SetDefine(UnityAnalyticsDefine, true);
            PlayerSettings.SetScriptingDefineSymbols(NamedBuildTarget.Android, _allDefines);
        }
        
        private static void EnableHuaweiAndroidPremiumDefines()
        {
            EditorUserBuildSettings.SwitchActiveBuildTarget(BuildTarget.Android);
            DisableAllOurDefines();
            SetDefine(HuaweiDefine, true);
            SetDefine(PremiumDefine, true);
            SetDefine(CollectorDefine, true);
            SetDefine(UnityAnalyticsDefine, true);
            PlayerSettings.SetScriptingDefineSymbols(NamedBuildTarget.Android, _allDefines);
        }
        
#region Utils
        [MenuItem(DefinesCheckMenuName)]
        private static void DefinesCheck()
        {
            var str = PlayerSettings.GetScriptingDefineSymbolsForGroup(EditorUserBuildSettings
                .selectedBuildTargetGroup);
            Debug.Log(str);
        }

        private static void DisableAllEditions()
        {
            _enabledGooglePlayAndroidF2PBuild = false;
            _enabled8FloorWindowsPremiumCe = false;
            _enabled8FloorMacOSPremiumCe = false;
            _enabledAndroidPremiumCeBuild = false;
            _enabledGooglePlayAndroidPremiumBuild = false;
            _enabledAppstoreIosPremiumBuild = false;
            _enabledAppstoreIosF2P = false;
            _enabledAppstoreMacosPremiumBuild = false;
            _enabledAppstoreMacosF2P = false;
        }

        private static void SetDefine(string defineName, bool value)
        {
            if (value)
            {
                if (!_allDefines.Contains($";{defineName}"))
                    _allDefines += $";{defineName}";
                else if (!_allDefines.Contains(defineName))
                    _allDefines += defineName;
            }
            else
            {
                if (_allDefines.Contains($";{defineName}"))
                    _allDefines = _allDefines.Replace($";{defineName}", "");
                else if (_allDefines.Contains(defineName))
                    _allDefines = _allDefines.Replace(defineName, "");
            }
        }

        private static void DisableAllOurDefines()
        {
            SetDefine(UdpDefine, false);
            SetDefine(GooglePlayDefine, false);
            SetDefine(PremiumDefine, false);
            SetDefine(CollectorDefine, false);
            SetDefine(HasAdDefine, false);
            SetDefine(UnityAnalyticsDefine, false);
            SetDefine(MacAppstoreDefine, false);
            SetDefine(HuaweiDefine, false);
        }
#endregion
    }
}
