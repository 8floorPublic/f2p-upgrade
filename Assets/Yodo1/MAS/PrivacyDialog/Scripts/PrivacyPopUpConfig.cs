﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Yodo1.MAS.PrivacyPolicy
{
    [CreateAssetMenu(fileName = "PrivacyPopUpConfig", menuName = "Yodo1/Privacy Pop Up/Edit Privacy config")]
    public class PrivacyPopUpConfig : ScriptableObject
    {
        public virtual void Initialize() { }        
        
        public enum DisplayLanguage {English, German, Spanish, French, Italian, Japanese, Korean, Portuguese, Russian, Chinese, ChineseTraditional}
        [Header("Content Language to display in privacy policy pop up in Unity Editor")]
        [SerializeField] public DisplayLanguage language;
        [Header("Edit English text contents in privacy policy pop up")]
        public bool EditEnglishText = false;
        [ConditionalHide("EditEnglishText", true)]
        public string TitleText = "How old are you?";
        [ConditionalHide("EditEnglishText", true)]
        public string AgeText = "Year(s) old";
        [ConditionalHide("EditEnglishText", true)]
        public string LabelText1 = "Please provide your age so we can optimize your game experience!";
        [ConditionalHide("EditEnglishText", true)]
        public string LabelText2 = "By clicking the \"I Agree\" button, you have read and agreed to our User Agreement and Privacy Policy.";
        [ConditionalHide("EditEnglishText", true)]
        public string UserAgreementText = "User Agreement";
        [ConditionalHide("EditEnglishText", true)]
        public string PrivacyPolicyText = "Privacy Policy";
        [ConditionalHide("EditEnglishText", true)]
        public string ButtonText = "I Agree";
        [ConditionalHide("EditEnglishText", true)]
        public string AgeWarningText = "You have not set your age yet.";

        [Header("Edit German text contents in privacy policy pop up")]
        public bool EditGermanText = false;
        [ConditionalHide("EditGermanText", true)]
        public string TitleTextGerman = "Wie alt bist du?";
        [ConditionalHide("EditGermanText", true)]
        public string AgeTextGerman = "Year(s) old";
        [ConditionalHide("EditGermanText", true)]
        public string LabelText1German = "Bitte gib dein Alter an, damit wir dein Spielerlebnis optimieren können!";
        [ConditionalHide("EditGermanText", true)]
        public string LabelText2German = "Durch Klicken des \"Ich stimme zu\" Buttons bestätigst du, die Nutzervereinbarung und Datenschutzregeln gelesen zu haben und ihnen zuzustimmen.";
        [ConditionalHide("EditGermanText", true)]
        public string UserAgreementTextGerman = "Nutzervereinbarung";
        [ConditionalHide("EditGermanText", true)]
        public string PrivacyPolicyTextGerman = "Datenschutzregeln";
        [ConditionalHide("EditGermanText", true)]
        public string ButtonTextGerman = "Ich stimme zu";
        [ConditionalHide("EditGermanText", true)]
        public string AgeWarningTextGerman = "Du hast dein Alter nicht eingestellt";

        [Header("Edit Spanish text contents in privacy policy pop up")]
        public bool EditSpanishText = false;
        [ConditionalHide("EditSpanishText", true)]
        public string TitleTextSpanish = "¿Cuántos años tienes?";
        [ConditionalHide("EditSpanishText", true)]
        public string AgeTextSpanish = "Años";
        [ConditionalHide("EditSpanishText", true)]
        public string LabelText1Spanish = "¡Por favor, indiques tu edad para que podamos optimizar tu experiencia de juego!";
        [ConditionalHide("EditSpanishText", true)]
        public string LabelText2Spanish = "Pulsando el botón de \"Estoy de acuerdo\" significa que has leído y aceptado nuestro Acuerdo de usuario y Política de privacidad";
        [ConditionalHide("EditSpanishText", true)]
        public string UserAgreementTextSpanish = "Acuerdo de usuario";
        [ConditionalHide("EditSpanishText", true)]
        public string PrivacyPolicyTextSpanish = "Política de privacidad";
        [ConditionalHide("EditSpanishText", true)]
        public string ButtonTextSpanish = "Estoy de acuerdo";
        [ConditionalHide("EditSpanishText", true)]
        public string AgeWarningTextSpanish = "Aún no has establecido tu edad";

        [Header("Edit French text contents in privacy policy pop up")]
        public bool EditFrenchText = false;
        [ConditionalHide("EditFrenchText", true)]
        public string TitleTextFrench = "Quel est votre âge?";
        [ConditionalHide("EditFrenchText", true)]
        public string AgeTextFrench = "ans";
        [ConditionalHide("EditFrenchText", true)]
        public string LabelText1French = "Veuillez entrer votre âge pour optimiser votre expérience de jeu!";
        [ConditionalHide("EditFrenchText", true)]
        public string LabelText2French = "En cliquant sur le bouton \"J'accepte\", vous reconnaissez avoir lu et accepté les Conditions d'utilisation et la Politique de confidentialité";
        [ConditionalHide("EditFrenchText", true)]
        public string UserAgreementTextFrench = "Conditions d'utilisation";
        [ConditionalHide("EditFrenchText", true)]
        public string PrivacyPolicyTextFrench = "Politique de confidentialité";
        [ConditionalHide("EditFrenchText", true)]
        public string ButtonTextFrench = "J'accepte";
        [ConditionalHide("EditFrenchText", true)]
        public string AgeWarningTextFrench = "Vous n'avez pas encore saisi votre âge"; 



        [Header("Edit Italian text contents in privacy policy pop up")]
        public bool EditItalianText = false;
        [ConditionalHide("EditItalianText", true)]
        public string TitleTextItalian = "Quanti anni hai?";
        [ConditionalHide("EditItalianText", true)]
        public string AgeTextItalian = "Anno(i)";
        [ConditionalHide("EditItalianText", true)]
        public string LabelText1Italian = "Si prega di fornire l'età in modo che possiamo ottimizzare la tua esperienza di gioco!";
        [ConditionalHide("EditItalianText", true)]
        public string LabelText2Italian = "Cliccando sul pulsante \"Accetto\", hai letto e accettato i nostri Accordo utenti e Informativa sulla privacy.";
        [ConditionalHide("EditItalianText", true)]
        public string UserAgreementTextItalian = "Accordo utenti";
        [ConditionalHide("EditItalianText", true)]
        public string PrivacyPolicyTextItalian = "Informativa sulla privacy";
        [ConditionalHide("EditItalianText", true)]
        public string ButtonTextItalian = "Accetto";
        [ConditionalHide("EditItalianText", true)]
        public string AgeWarningTextItalian = "Non hai ancora impostato la tua età";

        [Header("Edit Japanese text contents in privacy policy pop up")]
        public bool EditJapaneseText = false;
        [ConditionalHide("EditJapaneseText", true)]
        public string TitleTextJapanese = "おいくつですか。";
        [ConditionalHide("EditJapaneseText", true)]
        public string AgeTextJapanese = "歳 です。";
        [ConditionalHide("EditJapaneseText", true)]
        public string LabelText1Japanese = "最適なゲーミング体験をご提供できるように、ご年齢情報をご登録ください。";
        [ConditionalHide("EditJapaneseText", true)]
        public string LabelText2Japanese = "”同意します”ボタンをクリックすることによって、弊社のユーザー利用規約とプライバシーポリシーをお読みになった上、同意されたものと見なします。";
        [ConditionalHide("EditJapaneseText", true)]
        public string UserAgreementTextJapanese = "利用規約";
        [ConditionalHide("EditJapaneseText", true)]
        public string PrivacyPolicyTextJapanese = "プライバシーポリシー";
        [ConditionalHide("EditJapaneseText", true)]
        public string ButtonTextJapanese = "同意します";
        [ConditionalHide("EditJapaneseText", true)]
        public string AgeWarningTextJapanese = "ご年齢はまだ設定されていません。";

        [Header("Edit Korean text contents in privacy policy pop up")]
        public bool EditKoreanText = false;
        [ConditionalHide("EditKoreanText", true)]
        public string TitleTextKorean = "귀하의 나이는 어떻게 되십니까?";
        [ConditionalHide("EditKoreanText", true)]
        public string AgeTextKorean = "세";
        [ConditionalHide("EditKoreanText", true)]
        public string LabelText1Korean = "게임 환경 최적화를 위해 나이를 입력해 주십시오!";
        [ConditionalHide("EditKoreanText", true)]
        public string LabelText2Korean = "\"동의\" 버튼 클릭 시 이용약관 및 개인정보보호정책에 동의한 것으로 간주합니다.";
        [ConditionalHide("EditKoreanText", true)]
        public string UserAgreementTextKorean = " 이용약관";
        [ConditionalHide("EditKoreanText", true)]
        public string PrivacyPolicyTextKorean = "개인정보보호정책";
        [ConditionalHide("EditKoreanText", true)]
        public string ButtonTextKorean = "동의";
        [ConditionalHide("EditKoreanText", true)]
        public string AgeWarningTextKorean = "아직 나이를 입력하지 않았습니다";

        [Header("Edit Portuguese text contents in privacy policy pop up")]
        public bool EditPortugueseText = false;
        [ConditionalHide("EditPortugueseText", true)]
        public string TitleTextPortuguese = "Quantos anos você tem?";
        [ConditionalHide("EditPortugueseText", true)]
        public string AgeTextPortuguese = "anos de idade";
        [ConditionalHide("EditPortugueseText", true)]
        public string LabelText1Portuguese = "Informa sua idade para podermos otimizar sua experiência de jogo!";
        [ConditionalHide("EditPortugueseText", true)]
        public string LabelText2Portuguese = "Ao clicar no botão \"IConcordo\", você confirma que leu e aceita o Acordo de Usuário e a Política de Privacidade.";
        [ConditionalHide("EditPortugueseText", true)]
        public string UserAgreementTextPortuguese = "Acordo de Usuário";
        [ConditionalHide("EditPortugueseText", true)]
        public string PrivacyPolicyTextPortuguese = "Política de Privacidade";
        [ConditionalHide("EditPortugueseText", true)]
        public string ButtonTextPortuguese = "IConcordo";
        [ConditionalHide("EditPortugueseText", true)]
        public string AgeWarningTextPortuguese = "Você ainda não informou sua idade";

        [Header("Edit Russian text contents in privacy policy pop up")]
        public bool EditRussianText = false;
        [ConditionalHide("EditRussianText", true)]
        public string TitleTextRussian = "Сколько вам лет?";
        [ConditionalHide("EditRussianText", true)]
        public string AgeTextRussian = "Год лет";
        [ConditionalHide("EditRussianText", true)]
        public string LabelText1Russian = "Укажите свой возраст, и мы настроим игру под вас!";
        [ConditionalHide("EditRussianText", true)]
        public string LabelText2Russian = "Нажав на кнопку \"IСогласен\", вы утверждаете что прочитали и согласны с Пользовательское соглашение и Политика конфиденциальности.";
        [ConditionalHide("EditRussianText", true)]
        public string UserAgreementTextRussian = "Пользовательское соглашение";
        [ConditionalHide("EditRussianText", true)]
        public string PrivacyPolicyTextRussian = "Политика конфиденциальности";
        [ConditionalHide("EditRussianText", true)]
        public string ButtonTextRussian = "Согласен";
        [ConditionalHide("EditRussianText", true)]
        public string AgeWarningTextRussian = "Вы еще не указали возраст";

        [Header("Edit Chinese text contents in privacy policy pop up")]
        public bool EditChineseText = false;
        [ConditionalHide("EditChineseText", true)]
        public string TitleTextChinese = "您的年龄？";
        [ConditionalHide("EditChineseText", true)]
        public string AgeTextChinese = "岁";
        [ConditionalHide("EditChineseText", true)]
        public string LabelText1Chinese = "请提供您的年龄，以便我们优化您的游戏体验！";
        [ConditionalHide("EditChineseText", true)]
        public string LabelText2Chinese = "点击 \"同意\" 按钮,表示您已阅读并同意我们的 用户协议 和 隐私政策";
        [ConditionalHide("EditChineseText", true)]
        public string UserAgreementTextChinese = "用户协议";
        [ConditionalHide("EditChineseText", true)]
        public string PrivacyPolicyTextChinese = "隐私政策";
        [ConditionalHide("EditChineseText", true)]
        public string ButtonTextChinese = "同意";
        [ConditionalHide("EditChineseText", true)]
        public string AgeWarningTextChinese = "您还没有设置您的年龄";

        [Header("Edit Chinese Traditional text contents in privacy policy pop up")]
        public bool EditChineseTraditionalText = false;
        [ConditionalHide("EditChineseTraditionalText", true)]
        public string TitleTextTraditional = "您的年齡？";
        [ConditionalHide("EditChineseTraditionalText", true)]
        public string AgeTextTraditional = "歲";
        [ConditionalHide("EditChineseTraditionalText", true)]
        public string LabelText1Traditional = "請提供您的年齡，以便我們優化您的遊戲體驗！";
        [ConditionalHide("EditChineseTraditionalText", true)]
        public string LabelText2Traditional = "點擊 \"同意\" 按鈕,表示您已閱讀並同意我們的 用戶協議 和 隱私政策";
        [ConditionalHide("EditChineseTraditionalText", true)]
        public string UserAgreementTextTraditional = "用戶協議";
        [ConditionalHide("EditChineseTraditionalText", true)]
        public string PrivacyPolicyTextTraditional = "隱私政策";
        [ConditionalHide("EditChineseTraditionalText", true)]
        public string ButtonTextTraditional = "同意";
        [ConditionalHide("EditChineseTraditionalText", true)]
        public string AgeWarningTextTraditional = "您還沒有設置您的年齡";

        [Header("Customize the colors of the contents of privacy policy pop up")]
        public bool EnableCustomizedColors = false;
        [ConditionalHide("EnableCustomizedColors", true)]
        public Color titleBackgroundColor = Color.black;
        [ConditionalHide("EnableCustomizedColors", true)]
        public Color titleTextColor = Color.black;
        [ConditionalHide("EnableCustomizedColors", true)]
        public Color contentBackgroundColor = Color.black;
        [ConditionalHide("EnableCustomizedColors", true)]
        public Color contentTextColor = Color.black;
        [ConditionalHide("EnableCustomizedColors", true)]
        public Color hyperLinkTextColor = Color.black;
        [ConditionalHide("EnableCustomizedColors", true)]
        public Color buttonBackgroundColor = Color.black;
        [ConditionalHide("EnableCustomizedColors", true)]
        public Color buttonTextColor = Color.black;

        [Header("Custom Privacy links (optional) ")]
        [Tooltip("Enter your privacy policy link. Leave empty if you do not have a privacy policy")]
        public string privacyPolicyLink;
        [Tooltip("Enter your terms of services link. Leave empty if you do not have a terms of services.")]
        public string userAgreementLink;

        

    }
}