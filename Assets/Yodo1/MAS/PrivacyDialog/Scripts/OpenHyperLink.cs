﻿using UnityEngine;
using TMPro;
using UnityEngine.EventSystems;

[RequireComponent(typeof(TMP_Text))]
public class OpenHyperLink : MonoBehaviour, IPointerClickHandler
{
	private const string DefaultUrl = "https://gamepolicy.yodo1.com/privacy_policy_en.html";

	[SerializeField, Tooltip("The UI GameObject having the TextMesh Pro component.")]
	private TMP_Text text = default;

	public void OnPointerClick(PointerEventData eventData)
	{
		var linkIndex = TMP_TextUtilities.FindIntersectingLink(text, Input.mousePosition, null);
		if(linkIndex < 0)
        {
			return;
        }
		var linkId = text.textInfo.linkInfo[linkIndex].GetLinkID();

		string url = string.Empty;

		if(string.Equals(linkId,"url1"))
        {

            if (string.IsNullOrEmpty(Yodo1.MAS.PrivacyPolicy.Yodo1PrivacyPolicy.UserAgreementURL))
            {
                url = "https://gamepolicy.yodo1.com/terms_of_Service_en.html";
            }
            else
            {
				url = Yodo1.MAS.PrivacyPolicy.Yodo1PrivacyPolicy.UserAgreementURL;
			}

        }
		else if(string.Equals(linkId, "url2"))
        {
            if (string.IsNullOrEmpty(Yodo1.MAS.PrivacyPolicy.Yodo1PrivacyPolicy.PrivacyPolicyURL))
            {
                url = "https://gamepolicy.yodo1.com/privacy_policy_en.html";
            }
            else
            {
                url = Yodo1.MAS.PrivacyPolicy.Yodo1PrivacyPolicy.PrivacyPolicyURL;
            }

        }
        else
        {
			url = DefaultUrl;
		}
		int length = text.textInfo.linkInfo[linkIndex].linkTextLength;
		int firstIndex = text.textInfo.linkInfo[linkIndex].linkTextfirstCharacterIndex;
		for (int i = 0; i < length; ++i)
		{
			int charIndex = firstIndex + i;
			int meshIndex = text.textInfo.characterInfo[charIndex].materialReferenceIndex;
			
			int vertexIndex = text.textInfo.characterInfo[charIndex].vertexIndex;
			if (vertexIndex == 0)
            {
				continue;
            }
			Color32[] vertexColors = text.textInfo.meshInfo[meshIndex].colors32;
			vertexColors[vertexIndex + 0] = Color.grey;
            vertexColors[vertexIndex + 1] = Color.grey;
            vertexColors[vertexIndex + 2] = Color.grey;
            vertexColors[vertexIndex + 3] = Color.grey;
            
		
		}
		text.UpdateVertexData(TMP_VertexDataUpdateFlags.All);
		Application.OpenURL(url);
	}
}