using UnityEditor;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using Yodo1.MAS.PrivacyPolicy;
using TMPro;
using System.Collections;
using System;

namespace Yodo1.MAS.PrivacyPolicy
{
    public class Yodo1PrivacyPolicy : MonoBehaviour
    {
        public PrivacyPopUpConfig config;
        public static bool UseCustomPrivacyPolicy = false;
        [HideInInspector]
        public Slider AgeSlider;
        [HideInInspector]
        public Image MainBG;
        [HideInInspector]
        public Image TitleBG;
        [HideInInspector]
        public TMP_Text TitleText;
        [HideInInspector]
        public TMP_Text ContentText1;
        [HideInInspector]
        public TMP_Text ContentText2;
        [HideInInspector]
        public TMP_Text ContentText3;
        [HideInInspector]
        public Image ButtonBG;
        [HideInInspector]
        public Text ButtonText;
        [HideInInspector]
        public Text ToastText;
        [HideInInspector]
        public Image ToastTextBG;
        [HideInInspector]
        public static string UserAgreementURL;
        [HideInInspector]
        public static string PrivacyPolicyURL;
        [HideInInspector]
        public static GameObject PrivacyPopUpHolder;
        [HideInInspector]
        public static Canvas PrivacyPopUpCanvas;
        private static GameObject PrivacyPolicyPopUp;
        [HideInInspector]
        public TMP_FontAsset JapaneseFont, ChineseFont, ChineseTraditionalFont, KoreanFont;
        private const float PERFECT_SCREEN_WIDTH = 1000f;
        private static System.Action<int> onCallback;
        private string tempAgeText;
        private void Awake()
        {
            Init();
        }
        private void Update()
        {
            ResizePrivacyPopUp(transform);
        }
        private void Init()
        {
            string colorVal = string.Empty;
            if (config.EnableCustomizedColors)
            {
                MainBG.color = config.contentBackgroundColor;
                TitleBG.color = config.titleBackgroundColor;
                TitleText.color = config.titleTextColor;
                ContentText1.color = ContentText2.color = config.contentTextColor;
                ContentText3.color = config.hyperLinkTextColor;
                ButtonBG.color = config.buttonBackgroundColor;
                ButtonText.color = config.buttonTextColor;
                colorVal = "#" + ColorUtility.ToHtmlStringRGBA(config.contentTextColor);

            }
#if UNITY_EDITOR
            if (config.language == PrivacyPopUpConfig.DisplayLanguage.German)
            {
                ShowGermanLanguage(colorVal);
            }
            else if (config.language == PrivacyPopUpConfig.DisplayLanguage.Spanish)
            {
                ShowSpanishLanguage(colorVal);
            }
            else if (config.language == PrivacyPopUpConfig.DisplayLanguage.French)
            {
                ShowFrenchLanguage(colorVal);
            }
            else if (config.language == PrivacyPopUpConfig.DisplayLanguage.Italian)
            {
                ShowItalianLanguage(colorVal);
            }
            else if (config.language == PrivacyPopUpConfig.DisplayLanguage.Japanese)
            {
                ShowJapaneseLanguage(colorVal);
            }
            else if (config.language == PrivacyPopUpConfig.DisplayLanguage.Korean)
            {
                ShowKoreanLanguage(colorVal);
            }
            else if (config.language == PrivacyPopUpConfig.DisplayLanguage.Portuguese)
            {
                ShowPortugueseLanguage(colorVal);
            }
            else if (config.language == PrivacyPopUpConfig.DisplayLanguage.Russian)
            {
                ShowRussianLanguage(colorVal);
            }
            else if (config.language == PrivacyPopUpConfig.DisplayLanguage.Chinese)
            {
                ShowChineseLanguage(colorVal);
            }
            else if (config.language == PrivacyPopUpConfig.DisplayLanguage.ChineseTraditional)
            {
                ShowChineseTraditionalLanguage(colorVal);
            }
            else if (config.language == PrivacyPopUpConfig.DisplayLanguage.English)
            {
                ShowEnglishLanguage(colorVal);
            }
#endif

#if !UNITY_EDITOR
            if (Application.systemLanguage == SystemLanguage.German)
            {
                ShowGermanLanguage(colorVal);
            }
            else if (Application.systemLanguage == SystemLanguage.Spanish)
            {
                ShowSpanishLanguage(colorVal);
            }
            else if (Application.systemLanguage == SystemLanguage.French)
            {
                ShowFrenchLanguage(colorVal);
            }
            else if (Application.systemLanguage == SystemLanguage.Italian)
            {
                ShowItalianLanguage(colorVal);
            }
            else if (Application.systemLanguage == SystemLanguage.Japanese)
            {
                ShowJapaneseLanguage(colorVal);
            }
            else if (Application.systemLanguage == SystemLanguage.Korean)
            {
                ShowKoreanLanguage(colorVal);
            }
            else if (Application.systemLanguage == SystemLanguage.Portuguese)
            {
                ShowPortugueseLanguage(colorVal);
            }
            else if (Application.systemLanguage == SystemLanguage.Russian)
            {
                ShowRussianLanguage(colorVal);
            }
            else if(Application.systemLanguage == SystemLanguage.Chinese || Application.systemLanguage == SystemLanguage.ChineseSimplified)
            {
                ShowChineseLanguage(colorVal);
            }
            else if(Application.systemLanguage == SystemLanguage.ChineseTraditional)
            {
                ShowChineseTraditionalLanguage(colorVal);
            }
            else
            {
                ShowEnglishLanguage(colorVal);
            }
#endif
            UserAgreementURL = config.userAgreementLink;
            PrivacyPolicyURL = config.privacyPolicyLink;

        }
        private void ShowEnglishLanguage(string colorVal)
        {
            TitleText.text = config.TitleText;
            ContentText1.text = "0 " + config.AgeText;
            ContentText2.text = config.LabelText1;
            string[] firstSplit = config.LabelText2.Split(new string[] { config.UserAgreementText }, System.StringSplitOptions.None);
            string[] secondSplit = firstSplit[1].Split(new string[] { config.PrivacyPolicyText }, System.StringSplitOptions.None);
            if (config.EnableCustomizedColors)
            {
                ContentText3.text = "<color=" + colorVal + ">" + firstSplit[0] + "</color><link=url1><u>" + config.UserAgreementText + "</u></link><color=" + colorVal + ">" + secondSplit[0] + "</color><link=url2><u>" + config.PrivacyPolicyText + "</u></link><color=" + colorVal + ">" + secondSplit[1] + "</color>";
            }
            else
            {
                ContentText3.text = firstSplit[0] + "<link=url1><u>" + config.UserAgreementText + "</u></link>" + secondSplit[0] + "<link=url2><u>" + config.PrivacyPolicyText + "</u></link>" + secondSplit[1];

            }
            ButtonText.text = config.ButtonText;
            ToastText.text = config.AgeWarningText;
            tempAgeText = config.AgeText;
        }
        private void ShowGermanLanguage(string colorVal)
        {
            TitleText.text = config.TitleTextGerman;
            ContentText1.text = "0 " + config.AgeTextGerman;
            ContentText2.text = config.LabelText1German;
            string[] firstSplit = config.LabelText2German.Split(new string[] { config.UserAgreementTextGerman }, System.StringSplitOptions.None);
            string[] secondSplit = firstSplit[1].Split(new string[] { config.PrivacyPolicyTextGerman }, System.StringSplitOptions.None);
            if (config.EnableCustomizedColors)
            {
                ContentText3.text = "<color=" + colorVal + ">" + firstSplit[0] + "</color><link=url1><u>" + config.UserAgreementTextGerman + "</u></link><color=" + colorVal + ">" + secondSplit[0] + "</color><link=url2><u>" + config.PrivacyPolicyTextGerman + "</u></link><color=" + colorVal + ">" + secondSplit[1] + "</color>";
            }
            else
            {
                ContentText3.text = firstSplit[0] + "<link=url1><u>" + config.UserAgreementTextGerman + "</u></link>" + secondSplit[0] + "<link=url2><u>" + config.PrivacyPolicyTextGerman + "</u></link>" + secondSplit[1];

            }
            ButtonText.text = config.ButtonTextGerman;
            ToastText.text = config.AgeWarningTextGerman;
            tempAgeText = config.AgeTextGerman;
        }
        private void ShowSpanishLanguage(string colorVal)
        {
            TitleText.text = config.TitleTextSpanish;
            ContentText1.text = "0 " + config.AgeTextSpanish;
            ContentText2.text = config.LabelText1Spanish;
            string[] firstSplit = config.LabelText2Spanish.Split(new string[] { config.UserAgreementTextSpanish }, System.StringSplitOptions.None);
            string[] secondSplit = firstSplit[1].Split(new string[] { config.PrivacyPolicyTextSpanish }, System.StringSplitOptions.None);
            if (config.EnableCustomizedColors)
            {
                ContentText3.text = "<color=" + colorVal + ">" + firstSplit[0] + "</color><link=url1><u>" + config.UserAgreementTextSpanish + "</u></link><color=" + colorVal + ">" + secondSplit[0] + "</color><link=url2><u>" + config.PrivacyPolicyTextSpanish + "</u></link><color=" + colorVal + ">" + secondSplit[1] + "</color>";
            }
            else
            {
                ContentText3.text = firstSplit[0] + "<link=url1><u>" + config.UserAgreementTextSpanish + "</u></link>" + secondSplit[0] + "<link=url2><u>" + config.PrivacyPolicyTextSpanish + "</u></link>" + secondSplit[1];

            }
            ButtonText.text = config.ButtonTextSpanish;
            ToastText.text = config.AgeWarningTextSpanish;
            tempAgeText = config.AgeTextSpanish;
        }
        private void ShowFrenchLanguage(string colorVal)
        {
            TitleText.text = config.TitleTextFrench;
            ContentText1.text = "0 " + config.AgeTextFrench;
            ContentText2.text = config.LabelText1French;
            string[] firstSplit = config.LabelText2French.Split(new string[] { config.UserAgreementTextFrench }, System.StringSplitOptions.None);
            string[] secondSplit = firstSplit[1].Split(new string[] { config.PrivacyPolicyTextFrench }, System.StringSplitOptions.None);
            if (config.EnableCustomizedColors)
            {
                ContentText3.text = "<color=" + colorVal + ">" + firstSplit[0] + "</color><link=url1><u>" + config.UserAgreementTextFrench + "</u></link><color=" + colorVal + ">" + secondSplit[0] + "</color><link=url2><u>" + config.PrivacyPolicyTextFrench + "</u></link><color=" + colorVal + ">" + secondSplit[1] + "</color>";
            }
            else
            {
                ContentText3.text = firstSplit[0] + "<link=url1><u>" + config.UserAgreementTextFrench + "</u></link>" + secondSplit[0] + "<link=url2><u>" + config.PrivacyPolicyTextFrench + "</u></link>" + secondSplit[1];

            }
            ButtonText.text = config.ButtonTextFrench;
            ToastText.text = config.AgeWarningTextFrench;
            tempAgeText = config.AgeTextFrench;
        }
        private void ShowItalianLanguage(string colorVal)
        {
            TitleText.text = config.TitleTextItalian;
            ContentText1.text = "0 " + config.AgeTextItalian;
            ContentText2.text = config.LabelText1Italian;
            string[] firstSplit = config.LabelText2Italian.Split(new string[] { config.UserAgreementTextItalian }, System.StringSplitOptions.None);
            string[] secondSplit = firstSplit[1].Split(new string[] { config.PrivacyPolicyTextItalian }, System.StringSplitOptions.None);
            if (config.EnableCustomizedColors)
            {
                ContentText3.text = "<color=" + colorVal + ">" + firstSplit[0] + "</color><link=url1><u>" + config.UserAgreementTextItalian + "</u></link><color=" + colorVal + ">" + secondSplit[0] + "</color><link=url2><u>" + config.PrivacyPolicyTextItalian + "</u></link><color=" + colorVal + ">" + secondSplit[1] + "</color>";
            }
            else
            {
                ContentText3.text = firstSplit[0] + "<link=url1><u>" + config.UserAgreementTextItalian + "</u></link>" + secondSplit[0] + "<link=url2><u>" + config.PrivacyPolicyTextItalian + "</u></link>" + secondSplit[1];

            }
            ButtonText.text = config.ButtonTextFrench;
            ToastText.text = config.AgeWarningTextFrench;
            tempAgeText = config.AgeTextFrench;
        }
        private void ShowJapaneseLanguage(string colorVal)
        {

            TitleText.text = config.TitleTextJapanese;
            TitleText.font = JapaneseFont;
            ContentText1.text = "0 " + config.AgeTextJapanese;
            ContentText1.font = JapaneseFont;
            ContentText2.text = config.LabelText1Japanese;
            ContentText2.font = JapaneseFont;
            string[] firstSplit = config.LabelText2Japanese.Split(new string[] { config.UserAgreementTextJapanese }, System.StringSplitOptions.None);
            string[] secondSplit = firstSplit[1].Split(new string[] { config.PrivacyPolicyTextJapanese }, System.StringSplitOptions.None);
            ContentText3.font = JapaneseFont;
            if (config.EnableCustomizedColors)
            {
                ContentText3.text = "<color=" + colorVal + ">" + firstSplit[0] + "</color><link=url1><u>" + config.UserAgreementTextJapanese + "</u></link><color=" + colorVal + ">" + secondSplit[0] + "</color><link=url2><u>" + config.PrivacyPolicyTextJapanese + "</u></link><color=" + colorVal + ">" + secondSplit[1] + "</color>";
            }
            else
            {
                ContentText3.text = firstSplit[0] + "<link=url1><u>" + config.UserAgreementTextJapanese + "</u></link>" + secondSplit[0] + "<link=url2><u>" + config.PrivacyPolicyTextJapanese + "</u></link>" + secondSplit[1];
            }
            ButtonText.text = config.ButtonTextJapanese;
            ToastText.text = config.AgeWarningTextJapanese;
            tempAgeText = config.AgeTextJapanese;
        }
        private void ShowKoreanLanguage(string colorVal)
        {
            TitleText.text = config.TitleTextKorean;
            TitleText.font = KoreanFont;
            ContentText1.text = "0 " + config.AgeTextKorean;
            ContentText1.font = KoreanFont;
            ContentText2.text = config.LabelText1Korean;
            ContentText2.font = KoreanFont;
            string[] firstSplit = config.LabelText2Korean.Split(new string[] { config.UserAgreementTextKorean }, System.StringSplitOptions.None);
            string[] secondSplit = firstSplit[1].Split(new string[] { config.PrivacyPolicyTextKorean }, System.StringSplitOptions.None);
            ContentText3.font = KoreanFont;
            if (config.EnableCustomizedColors)
            {
                ContentText3.text = "<color=" + colorVal + ">" + firstSplit[0] + "</color><link=url1><u>" + config.UserAgreementTextKorean + "</u></link><color=" + colorVal + ">" + secondSplit[0] + "</color><link=url2><u>" + config.PrivacyPolicyTextKorean + "</u></link><color=" + colorVal + ">" + secondSplit[1] + "</color>";
            }
            else
            {
                ContentText3.text = firstSplit[0] + "<link=url1><u>" + config.UserAgreementTextKorean + "</u></link>" + secondSplit[0] + "<link=url2><u>" + config.PrivacyPolicyTextKorean + "</u></link>" + secondSplit[1];
            }
            ButtonText.text = config.ButtonTextKorean;
            ToastText.text = config.AgeWarningTextKorean;
            tempAgeText = config.AgeTextKorean;
        }
        private void ShowPortugueseLanguage(string colorVal)
        {
            TitleText.text = config.TitleTextPortuguese;
            ContentText1.text = "0 " + config.AgeTextPortuguese;
            ContentText2.text = config.LabelText1Portuguese;
            string[] firstSplit = config.LabelText2Portuguese.Split(new string[] { config.UserAgreementTextPortuguese }, System.StringSplitOptions.None);
            string[] secondSplit = firstSplit[1].Split(new string[] { config.PrivacyPolicyTextPortuguese }, System.StringSplitOptions.None);
            if (config.EnableCustomizedColors)
            {
                ContentText3.text = "<color=" + colorVal + ">" + firstSplit[0] + "</color><link=url1><u>" + config.UserAgreementTextPortuguese + "</u></link><color=" + colorVal + ">" + secondSplit[0] + "</color><link=url2><u>" + config.PrivacyPolicyTextPortuguese + "</u></link><color=" + colorVal + ">" + secondSplit[1] + "</color>";
            }
            else
            {
                ContentText3.text = firstSplit[0] + "<link=url1><u>" + config.UserAgreementTextPortuguese + "</u></link>" + secondSplit[0] + "<link=url2><u>" + config.PrivacyPolicyTextPortuguese + "</u></link>" + secondSplit[1];

            }
            ButtonText.text = config.ButtonTextPortuguese;
            ToastText.text = config.AgeWarningTextPortuguese;
            tempAgeText = config.AgeTextPortuguese;
        }
        private void ShowRussianLanguage(string colorVal)
        {
            TitleText.text = config.TitleTextRussian;
            ContentText1.text = "0 " + config.AgeTextRussian;
            ContentText2.text = config.LabelText1Russian;
            string[] firstSplit = config.LabelText2Russian.Split(new string[] { config.UserAgreementTextRussian }, System.StringSplitOptions.None);
            string[] secondSplit = firstSplit[1].Split(new string[] { config.PrivacyPolicyTextRussian }, System.StringSplitOptions.None);
            if (config.EnableCustomizedColors)
            {
                ContentText3.text = "<color=" + colorVal + ">" + firstSplit[0] + "</color><link=url1><u>" + config.UserAgreementTextRussian + "</u></link><color=" + colorVal + ">" + secondSplit[0] + "</color><link=url2><u>" + config.PrivacyPolicyTextRussian + "</u></link><color=" + colorVal + ">" + secondSplit[1] + "</color>";
            }
            else
            {
                ContentText3.text = firstSplit[0] + "<link=url1><u>" + config.UserAgreementTextRussian + "</u></link>" + secondSplit[0] + "<link=url2><u>" + config.PrivacyPolicyTextRussian + "</u></link>" + secondSplit[1];

            }
            ButtonText.text = config.ButtonTextRussian;
            ToastText.text = config.AgeWarningTextRussian;
            tempAgeText = config.AgeTextRussian;
        }

        private void ShowChineseLanguage(string colorVal)
        {
            TitleText.text = config.TitleTextChinese;
            TitleText.font = ChineseFont;
            ContentText1.text = "0 " + config.AgeTextChinese;
            ContentText1.font = ChineseFont;
            ContentText2.text = config.LabelText1Chinese;
            ContentText2.font = ChineseFont;
            string[] firstSplit = config.LabelText2Chinese.Split(new string[] { config.UserAgreementTextChinese }, System.StringSplitOptions.None);
            string[] secondSplit = firstSplit[1].Split(new string[] { config.PrivacyPolicyTextChinese }, System.StringSplitOptions.None);
            ContentText3.font = ChineseFont;
            if (config.EnableCustomizedColors)
            {
                ContentText3.text = "<color=" + colorVal + ">" + firstSplit[0] + "</color><link=url1><u>" + config.UserAgreementTextChinese + "</u></link><color=" + colorVal + ">" + secondSplit[0] + "</color><link=url2><u>" + config.PrivacyPolicyTextChinese + "</u></link><color=" + colorVal + ">" + secondSplit[1] + "</color>";
            }
            else
            {
                ContentText3.text = firstSplit[0] + "<link=url1><u>" + config.UserAgreementTextChinese + "</u></link>" + secondSplit[0] + "<link=url2><u>" + config.PrivacyPolicyTextChinese + "</u></link>" + secondSplit[1];
            }
            ButtonText.text = config.ButtonTextChinese;
            ToastText.text = config.AgeWarningTextChinese;
            tempAgeText = config.AgeTextChinese;
        }
        private void ShowChineseTraditionalLanguage(string colorVal)
        {
            TitleText.text = config.TitleTextTraditional;
            TitleText.font = ChineseTraditionalFont;
            ContentText1.text = "0 " + config.AgeTextTraditional;
            ContentText1.font = ChineseTraditionalFont;
            ContentText2.text = config.LabelText1Traditional;
            ContentText2.font = ChineseTraditionalFont;
            string[] firstSplit = config.LabelText2Traditional.Split(new string[] { config.UserAgreementTextTraditional }, System.StringSplitOptions.None);
            string[] secondSplit = firstSplit[1].Split(new string[] { config.PrivacyPolicyTextTraditional }, System.StringSplitOptions.None);
            ContentText3.font = ChineseTraditionalFont;
            if (config.EnableCustomizedColors)
            {
                ContentText3.text = "<color=" + colorVal + ">" + firstSplit[0] + "</color><link=url1><u>" + config.UserAgreementTextTraditional + "</u></link><color=" + colorVal + ">" + secondSplit[0] + "</color><link=url2><u>" + config.PrivacyPolicyTextTraditional + "</u></link><color=" + colorVal + ">" + secondSplit[1] + "</color>";
            }
            else
            {
                ContentText3.text = firstSplit[0] + "<link=url1><u>" + config.UserAgreementTextTraditional + "</u></link>" + secondSplit[0] + "<link=url2><u>" + config.PrivacyPolicyTextTraditional + "</u></link>" + secondSplit[1];
            }
            ButtonText.text = config.ButtonTextTraditional;
            ToastText.text = config.AgeWarningTextTraditional;
            tempAgeText = config.AgeTextTraditional;
        }


        public void OnAgeChanged()
        {
            ContentText1.text = "<b>" + AgeSlider.value + "</b> " + tempAgeText;

        }
        public void OnUserAgreeMentClicked()
        {
            if (string.IsNullOrEmpty(UserAgreementURL))
            {
                Application.OpenURL("https://gamepolicy.yodo1.com/terms_of_Service_en.html");
            }
            else
            {
                Application.OpenURL(UserAgreementURL);
            }

        }
        public void OnPrivacyPolicyClicked()
        {
            if (string.IsNullOrEmpty(PrivacyPolicyURL))
            {
                Application.OpenURL("https://gamepolicy.yodo1.com/privacy_policy_en.html");
            }
            else
            {
                Application.OpenURL(PrivacyPolicyURL);
            }
        }
        public void OnAgreeButtonClicked()
        {
            if (AgeSlider.value == 0)
            {
                showToast(1);
            }
            else
            {
                PlayerPrefs.SetFloat("UserAge", AgeSlider.value);
                PlayerPrefs.SetInt("PrivacyPopUpShown", 1);
                DestroyPrivacyPopUp();

                if (onCallback != null)
                {
                    onCallback((int)AgeSlider.value);
                }
            }
        }

        public static void ShowPrivacyPopUp(System.Action<int> action)
        {
            onCallback = action;
            if (!HasPrivacyPopUpShown())
            {
                EventSystem sceneEventSystem = FindObjectOfType<EventSystem>();
                if (PrivacyPopUpHolder == null)
                {
                    PrivacyPopUpHolder = Instantiate(Resources.Load("PrivacyPolicyHolder") as GameObject);
                    PrivacyPopUpHolder.name = "Yodo1PrivacyPolicyCanvas";
                    PrivacyPopUpCanvas = PrivacyPopUpHolder.transform.GetChild(0).GetComponent<Canvas>();
                    PrivacyPopUpCanvas.sortingOrder = HighestOrderCanvas();
                }
                if (sceneEventSystem == null)
                {
                    var eventSystem = new GameObject("EventSystem", typeof(EventSystem), typeof(StandaloneInputModule));
                }
                if (PrivacyPolicyPopUp == null)
                {
                    PrivacyPolicyPopUp = Instantiate(Resources.Load("PrivacyPolicyPopUp") as GameObject, PrivacyPopUpCanvas.transform);
                }
            }
            else
            {
                if (onCallback != null)
                {
                    onCallback((int)PlayerPrefs.GetFloat("UserAge", 0));
                }
            }

        }
        public static void DestroyPrivacyPopUp()
        {
            Destroy(PrivacyPolicyPopUp);
        }
        public static bool HasPrivacyPopUpShown()
        {
            if (PlayerPrefs.GetInt("PrivacyPopUpShown", 0) == 0)
            {
                return false;
            }
            else
            {
                return true;
            }

        }
        public static void ResizePrivacyPopUp(Transform panelTransform)
        {
            Vector2 size = GetCanvasReferenceResolution();
            if (size.x < PERFECT_SCREEN_WIDTH)
            {
                float scaler = size.x / PERFECT_SCREEN_WIDTH;
                panelTransform.localScale = Vector3.one * (scaler - 0.05f);
            }

        }
        public static Vector2 GetCanvasReferenceResolution()
        {
            if (PrivacyPopUpCanvas != null)
            {
                RectTransform rect = PrivacyPopUpCanvas.transform.GetComponentInParent<RectTransform>();
                return rect.sizeDelta;
            }
            else
            {
                return new Vector2(Screen.width, Screen.height);
            }
        }
        public static int HighestOrderCanvas()
        {
            Canvas[] canvases = GameObject.FindObjectsOfType<Canvas>();
            int length = canvases.Length;
            int highestOrder = canvases[0].sortingOrder;
            for (int i = 1; i < length; i++)
            {
                if (highestOrder < canvases[i].sortingOrder)
                {
                    highestOrder = canvases[i].sortingOrder;
                }
            }
            return highestOrder + 2;
        }
        void showToast(int duration)
        {
            StartCoroutine(showToastCOR(duration));
        }

        private IEnumerator showToastCOR(int duration)
        {
            ToastTextBG.gameObject.SetActive(true);
            Color orginalColor = ToastText.color;
            Color orginalColorBG = ToastTextBG.color;

            ToastText.enabled = true;
            //Fade in
            yield return fadeInAndOut(ToastText, ToastTextBG, true, 0.5f);
            //Wait for the duration
            float counter = 0;
            while (counter < duration)
            {
                counter += Time.deltaTime;
                yield return null;
            }
            //Fade out
            yield return fadeInAndOut(ToastText, ToastTextBG, false, 0.5f);
            ToastTextBG.gameObject.SetActive(false);
            ToastText.enabled = false;
            ToastText.color = orginalColor;
        }

        IEnumerator fadeInAndOut(Text targetText, Image targetImage, bool fadeIn, float duration)
        {
            //Set Values depending on if fadeIn or fadeOut
            float a, b;
            if (fadeIn)
            {
                a = 0f;
                b = 1f;
            }
            else
            {
                a = 1f;
                b = 0f;
            }

            Color currentColor = Color.clear;
            float counter = 0f;

            while (counter < duration)
            {
                counter += Time.deltaTime;
                float alpha = Mathf.Lerp(a, b, counter / duration);
                //targetImage.color = new Color(currentColor.r, currentColor.g, currentColor.b, alpha);
                targetText.color = new Color(currentColor.r, currentColor.g, currentColor.b, alpha);
                yield return null;
            }

        }

        private string ReverseFont(string s)
        {
            char[] charArray = s.ToCharArray();
            Array.Reverse(charArray);
            return new string(charArray);
        }
#if UNITY_EDITOR
        [MenuItem("Yodo1/PrivacyDialog/Privacy Config")]
        static void EditPrivacyConfig()
        {
            var configPath = AssetDatabase.LoadMainAssetAtPath($"Assets/Yodo1/MAS/PrivacyDialog/Resources/PrivacyPopUpConfig.asset");
            AssetDatabase.OpenAsset(configPath);
        }
        [MenuItem("Yodo1/PrivacyDialog/Edit Privacy Pop Up")]
        static void EditPrivacyPopUp()
        {
            var configPath = AssetDatabase.LoadMainAssetAtPath($"Assets/Yodo1/MAS/PrivacyDialog/Resources/PrivacyPolicyPopUp.prefab");
            AssetDatabase.OpenAsset(configPath);
        }
        [MenuItem("Yodo1/PrivacyDialog/Clear Privacy Plugin Prefs")]
        static void ClearPrivacyPopUpPrefs()
        {
            PlayerPrefs.DeleteKey("PrivacyPopUpShown");
            PlayerPrefs.DeleteKey("UserAge");
        }
#endif
    }

}
